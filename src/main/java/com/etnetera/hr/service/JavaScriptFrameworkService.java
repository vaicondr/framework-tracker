package com.etnetera.hr.service;

import com.etnetera.hr.data.JavaScriptFramework;

public interface JavaScriptFrameworkService {
    Iterable<JavaScriptFramework> findAll();
    JavaScriptFramework addFramework(JavaScriptFramework javaScriptFramework);
    boolean javaScriptFrameworkAlreadyPresent(JavaScriptFramework javaScriptFramework);
    JavaScriptFramework editFramework(Long frameworkIndex, JavaScriptFramework javaScriptFramework);
    JavaScriptFramework findJavaScriptFrameworkByNameAndRunningVersion(String name, String version);
    Iterable<JavaScriptFramework> findJavaScriptFrameworkByName(String name);
    void deleteFrameworkById(Long frameworkIndex);
}
