package com.etnetera.hr.service;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JavaScriptFrameworkServiceImpl implements JavaScriptFrameworkService{
    private final JavaScriptFrameworkRepository repository;


    @Autowired
    public JavaScriptFrameworkServiceImpl(JavaScriptFrameworkRepository repository) {
        this.repository = repository;
    }

    public Iterable<JavaScriptFramework> findAll() {
        return repository.findAll();
    }

    public JavaScriptFramework addFramework(JavaScriptFramework javaScriptFramework) {
        return repository.save(javaScriptFramework);
    }

    public JavaScriptFramework editFramework(Long frameworkIndex, JavaScriptFramework javaScriptFramework) {
        Optional<JavaScriptFramework> oldFramework = repository.findById(frameworkIndex);
        return oldFramework.map(scriptFramework -> repository.save(updateFramework(scriptFramework, javaScriptFramework)))
                .orElse(null);
    }

    private JavaScriptFramework updateFramework(JavaScriptFramework oldFramework, JavaScriptFramework updatedFramework) {
        oldFramework.setRunningVersion(updatedFramework.getRunningVersion());
        oldFramework.setHypeLevel(updatedFramework.getHypeLevel());
        oldFramework.setDeprecationDate(updatedFramework.getDeprecationDate());
        oldFramework.setName(updatedFramework.getName());
        return oldFramework;
    }


    public boolean javaScriptFrameworkAlreadyPresent(JavaScriptFramework javaScriptFramework) {
        return repository.findJavaScriptFrameworkByNameAndRunningVersion(javaScriptFramework.getName(),
                javaScriptFramework.getRunningVersion()) != null;
    }

    public JavaScriptFramework findJavaScriptFrameworkByNameAndRunningVersion(String name, String version) {
        return repository.findJavaScriptFrameworkByNameAndRunningVersion(name, version);
    }

    public Iterable<JavaScriptFramework> findJavaScriptFrameworkByName(String name) {
        return repository.findJavaScriptFrameworkByName(name);
    }

    public void deleteFrameworkById(Long frameworkIndex) {
        if(!repository.findById(frameworkIndex).isPresent()) {
            return;
        }
        repository.deleteById(frameworkIndex);
    }
}
