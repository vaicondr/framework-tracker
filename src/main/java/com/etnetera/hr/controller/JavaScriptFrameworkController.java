package com.etnetera.hr.controller;

import com.etnetera.hr.service.JavaScriptFrameworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.etnetera.hr.data.JavaScriptFramework;

import javax.validation.Valid;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController extends EtnRestController {

	private final JavaScriptFrameworkService service;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkService service) {
		this.service = service;
	}

	@GetMapping("/frameworks")
	public Iterable<JavaScriptFramework> frameworks() {
		return service.findAll();
	}

	@PostMapping("/add")
	public ResponseEntity<JavaScriptFramework> addFramework(@Valid @RequestBody JavaScriptFramework javaScriptFramework) {
		if(service.javaScriptFrameworkAlreadyPresent(javaScriptFramework)) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
		return new ResponseEntity<>(service.addFramework(javaScriptFramework) ,HttpStatus.OK);
	}

	@PutMapping("/framework/{id}")
	public ResponseEntity<JavaScriptFramework> editFramework(@PathVariable("id") Long frameworkIndex,
										@Valid @RequestBody JavaScriptFramework javaScriptFramework) {
		return new ResponseEntity<>(service.editFramework(frameworkIndex, javaScriptFramework), HttpStatus.OK);
	}


	@GetMapping(value = "/framework", params = {"name", "version"})
	public ResponseEntity<JavaScriptFramework> framework(@RequestParam String name, @RequestParam String version) {
		return new ResponseEntity<>(service.findJavaScriptFrameworkByNameAndRunningVersion(name, version), HttpStatus.OK);
	}

	@GetMapping(value = "/framework", params = {"name"})
	public Iterable<JavaScriptFramework> framework(@RequestParam String name) {
		return service.findJavaScriptFrameworkByName(name);
	}

	@DeleteMapping("/framework/{id}")
	public ResponseEntity deleteFrameworkById(@PathVariable("id") Long frameworkIndex) {
		service.deleteFrameworkById(frameworkIndex);
		return ResponseEntity.ok(HttpStatus.OK);
	}


}
