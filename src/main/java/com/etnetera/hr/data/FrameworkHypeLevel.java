package com.etnetera.hr.data;

public enum FrameworkHypeLevel {
    HATED, BORING, CONTROVERSIAL, FUN, EXCITING, LOVED
}
