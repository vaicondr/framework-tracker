package com.etnetera.hr.data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 30)
	@Size(min = 1, max=30)
	@NotEmpty(message = "Provide a name for framework")
	private String name;

	@Column
	private Long deprecationDate;

	@Column(nullable = false, length = 30)
	@NotEmpty(message = "Provide hype level for framework")
	private String hypeLevel;

	@Column(nullable = false, length = 30)
	@Size(min = 1, max=30)
	@NotEmpty(message = "Provide a version for framework")
	private String runningVersion;

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name, String runningVersion, Long deprecationDate,
							   FrameworkHypeLevel hypeLevel) {
		this.name = name;
		this.runningVersion = runningVersion;
		this.deprecationDate = deprecationDate;
		this.hypeLevel = hypeLevel.toString();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRunningVersion() {
		return runningVersion;
	}

	public void setRunningVersion(String runningVersion) {
		this.runningVersion = runningVersion;
	}

	public Long getDeprecationDate() {
		return deprecationDate;
	}

	public void setDeprecationDate(Long deprecationDate) {
		this.deprecationDate = deprecationDate;
	}

	public String getHypeLevel() {
		return this.hypeLevel;
	}

	public void setHypeLevel(String hypeLevel) {
		this.hypeLevel = hypeLevel;
	}

	public static JavaScriptFrameworkEntityBuilder builder() {
		return new JavaScriptFrameworkEntityBuilder();
	}

	@Override
	public String toString() {
		return "JavaScriptFramework{" +
				"id=" + id +
				", name='" + name + '\'' +
				", deprecationDate=" + deprecationDate +
				", hypeLevel='" + hypeLevel + '\'' +
				", runningVersions=" + runningVersion +
				'}';
	}

	public static class JavaScriptFrameworkEntityBuilder {

		private String frameworkName;
		private String versions;
		private long deprecationDate;
		private FrameworkHypeLevel hypeLevel;

		public JavaScriptFrameworkEntityBuilder setFrameworkName(String name) {
			this.frameworkName = name;
			return this;
		}

		public JavaScriptFrameworkEntityBuilder setVersions(String versions) {
			this.versions = versions;
			return this;
		}

		public JavaScriptFrameworkEntityBuilder setDeprecationDate(long deprecationDate) {
			this.deprecationDate = deprecationDate;
			return this;
		}

		public JavaScriptFrameworkEntityBuilder setHypeLevel(FrameworkHypeLevel hypeLevel) {
			this.hypeLevel = hypeLevel;
			return this;
		}

		public JavaScriptFramework build() {
			return new JavaScriptFramework(frameworkName, versions, deprecationDate, hypeLevel);
		}

	}

}
