package com.etnetera.hr;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.etnetera.hr.data.FrameworkHypeLevel;
import com.etnetera.hr.helpers.TestingEntities;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JavaScriptFrameworkTests {

	@Autowired
	private MockMvc mockMvc;
	
	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private JavaScriptFrameworkRepository repository;


	private void prepareData() throws Exception {
		JavaScriptFramework react = TestingEntities.buildReactFramework();
		JavaScriptFramework vue = TestingEntities.buildVueFramework();
		JavaScriptFramework angular = TestingEntities.buildAngularFramework();
		
		repository.save(react);
		repository.save(vue);
		repository.save(angular);
	}

	@Test
	public void frameworksTest() throws JsonProcessingException, Exception {
		prepareData();

		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is(TestingEntities.REACT)))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is(TestingEntities.VUE)))
				.andExpect(jsonPath("$[2].id", is(3)))
				.andExpect(jsonPath("$[2].name", is(TestingEntities.ANGULAR)));
	}

	@Test
	public void addFrameworkValid() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = TestingEntities.buildReactFramework();
		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(0)));
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isOk());
		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(1)));
	}
	
	@Test
	public void addFrameworkInvalid() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = new JavaScriptFramework();
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errors", hasSize(3)))
				.andExpect(jsonPath("$.errors[0].message", is("NotEmpty")))
				.andExpect(jsonPath("$.errors[1].message", is("NotEmpty")))
				.andExpect(jsonPath("$.errors[2].message", is("NotEmpty")));

		framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
		framework.setHypeLevel(FrameworkHypeLevel.FUN.toString());
		framework.setRunningVersion("1.0");

		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errors", hasSize(1)))
				.andExpect(jsonPath("$.errors[0].field", is("name")))
				.andExpect(jsonPath("$.errors[0].message", is("Size")));

	}

	@Test
	public void addFrameworksWithSameNameAndVersion() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = TestingEntities.buildReactFramework();
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)));
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isConflict());

	}

	@Test
	public void findByNameValid() throws JsonProcessingException, Exception {
		prepareData();
		mockMvc.perform(get("/framework").param("name", TestingEntities.REACT))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)));

		JavaScriptFramework framework = TestingEntities.buildReactFramework();
		framework.setRunningVersion("2.0");
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)));
		mockMvc.perform(get("/framework").param("name", TestingEntities.REACT))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void findByNameInvalid() throws JsonProcessingException, Exception {
		mockMvc.perform(get("/framework").param("name", TestingEntities.REACT))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	public void findByNameAndVersionValid() throws JsonProcessingException, Exception {
		prepareData();
		mockMvc.perform(get("/framework")
				.param("name", TestingEntities.REACT)
				.param("version", "1.0"))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString(TestingEntities.REACT)))
				.andExpect(content().string(containsString("1.0")));

	}

	@Test
	public void findByNameAndVersionInvalid() throws JsonProcessingException, Exception {
		prepareData();
		mockMvc.perform(get("/framework")
				.param("name", TestingEntities.REACT)
				.param("version", "15.0"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}

	@Test
	public void deleteFrameworkSuccesful() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = TestingEntities.buildReactFramework();
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isOk());

		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(1)));
		mockMvc.perform(delete("/framework/{id}",1L))
				.andExpect(status().isOk());
		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	public void deleteInvalidFramework() throws JsonProcessingException, Exception  {
		mockMvc.perform(delete("/framework/{id}",1L))
				.andExpect(status().isOk());
	}

	@Test
	public void editFrameworkValid() throws  JsonProcessingException, Exception {
		prepareData();
		mockMvc.perform(get("/framework").param("name", TestingEntities.REACT))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)));

		JavaScriptFramework framework = TestingEntities.buildReactFramework();
		String runningVersion = "2.2";
		framework.setRunningVersion(runningVersion);
		mockMvc.perform(put("/framework/{id}",1L).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString(runningVersion)));

		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is(TestingEntities.REACT)))
				.andExpect(jsonPath("$[0].runningVersion", is(runningVersion)));



	}

	@Test
	public void editFrameworkInvalid() throws JsonProcessingException, Exception {
		mockMvc.perform(get("/framework").param("name", TestingEntities.REACT))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));
		JavaScriptFramework framework = TestingEntities.buildReactFramework();
		mockMvc.perform(put("/framework/{id}",1L).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());

		JavaScriptFramework invalidFramework = new JavaScriptFramework();
		mockMvc.perform(put("/framework/{id}",1L).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsBytes(invalidFramework)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errors", hasSize(3)))
				.andExpect(jsonPath("$.errors[0].message", is("NotEmpty")))
				.andExpect(jsonPath("$.errors[1].message", is("NotEmpty")))
				.andExpect(jsonPath("$.errors[2].message", is("NotEmpty")));
	}

}
