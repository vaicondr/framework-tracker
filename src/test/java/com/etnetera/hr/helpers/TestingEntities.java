package com.etnetera.hr.helpers;

import com.etnetera.hr.data.FrameworkHypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class TestingEntities {
    public static final String REACT = "ReactJS";
    public static final String VUE = "Vue.js";
    public static final String ANGULAR = "Angular.js";

    public static JavaScriptFramework buildReactFramework() {
        return JavaScriptFramework.builder()
                .setFrameworkName(REACT)
                .setVersions("1.0")
                .setHypeLevel(FrameworkHypeLevel.LOVED)
                .build();
    }

    public static JavaScriptFramework buildVueFramework() {
        return JavaScriptFramework.builder()
                .setFrameworkName(VUE)
                .setVersions("1.0")
                .setHypeLevel(FrameworkHypeLevel.FUN)
                .build();
    }

    public static JavaScriptFramework buildAngularFramework() throws ParseException {
        return JavaScriptFramework.builder()
                .setFrameworkName(ANGULAR)
                .setVersions("27.1.2")
                .setDeprecationDate(new SimpleDateFormat("yyyy-MM-dd").parse("2016-04-12").getTime())
                .setHypeLevel(FrameworkHypeLevel.BORING)
                .build();
    }

}
